@echo off
setlocal enabledelayedexpansion
echo Repeat run test_ftp for many times
if {%1}=={} goto usage
set fn=do.bat
if {%2} NEQ {} set fn=%2
for /L %%a in (1,1,%1) do (
	echo Repeat Test_ftp No: %%a% tests
	call %fn%
)
goto end
:usage
echo Air720 Test FTP Tool Repeat Test Tool
echo usage:  don times
echo     where times is a number you want to
echo   repeat.
echo Example: don 10
echo   repeat test_ftp for 10 times
:end

