# coding:utf-8
# !usr/bin/python3.6

import serial
import sys
import time
import filecmp
import yaml
import logging.config
import os


# global MAX_LOOP_NUM
# global newCmd
# global sbuf
# sbuf=''
# MAX_LOOP_NUM = 10

def setup_logging(default_path="logging.yaml", default_level=logging.INFO, env_key="LOG_CFG"):
    """
    
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, "r") as f:
            config = yaml.load(f)
            logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def sendInfo(strInfo):
    try:
        # print ('['+time.strftime("%Y-%m-%d %X")+']发→◇'+strInfo.decode())
        logging.info('发→◇' + repr(strInfo))
    except Exception:
        # print('['+time.strftime("%Y-%m-%d %X")+']发→◇', end='')
        # print(strInfo)
        logging.info('发→◇' + repr(strInfo))


def recvInfo(strInfo):
    try:
        # print ('['+time.strftime("%Y-%m-%d %X")+']收←◆'+strInfo.decode())
        logging.info('收←◆' + repr(strInfo))
    except Exception:
        # print ('['+time.strftime("%Y-%m-%d %X")+']收←◆', end='')
        # print(strInfo)
        logging.info('except收←◆' + repr(strInfo))


# import serial.tools.list_ports
def list_comport():
    plist = list(serial.tools.list_ports.comports())
    if len(plist) <= 0:
        print("没有发现端口!")
    else:
        plist_0 = list(plist[0])
        serialName = plist_0[0]
        try:
            serialFd = serial.Serial(serialName, 9600, timeout=60)
            print("可用端口名>>>", serialFd.name)
            return serialFd.name
        except:
            print('无法打开串口')
            sys.exit(0)


def waitForCmdOKRsp(timeout):
    # maxloopNum = 0
    timeout *= 10
    # print("Rsponse :")
    while True:
        n = ser.inWaiting()
        if n:
            line = ser.read(n)
            recvInfo(line)
            # maxloopNum = maxloopNum + 1
            i = line.find(b'OK')
            if i >= 0:
                # print('find ok')
                break
        time.sleep(0.1)
        timeout -= 1
        if (timeout <= 0):
            # print('[INFO]等待AT命令返回OK超时')
            logging.error("等待AT命令返回OK超时")
            sys.exit(0)


def waitForCmdRsp(timeout):
    # maxloopNum = 0
    timeout *= 10
    # print("Rsponse :")
    while True:
        n = ser.inWaiting()
        if n:
            line = ser.read(n)
            recvInfo(line)
            return
        # maxloopNum = maxloopNum + 1

        time.sleep(0.1)
        timeout -= 1
        if (timeout <= 0):
            # print('[INFO]等待AT命令返回超时')
            logging.error("等待AT命令返回OK超时")
            break


def sendAT_Cmd(serInstance, atCmdStr, waitforOk, timeout):
    # print("SendCmd: %s" % atCmdStr)
    sendInfo(atCmdStr)
    serInstance.write(atCmdStr.encode('utf-8'))
    # or define b'string',bytes should be used not str
    if (waitforOk == 1):
        waitForCmdOKRsp(timeout)
    else:
        waitForCmdRsp(timeout)


def waitForResp(respStr, timeout):
    # print('Enter waitForResp')
    timeout *= 10
    while True:
        n = ser.inWaiting()
        if n:
            line = ser.read(n).decode()
            recvInfo(line)
            rep = line.split('\r\n')
            for i in range(len(rep) - 1, -1, -1):
                if rep[i] == '':
                    rep.remove('')
            i = rep[0].find(respStr)
            if i == 0:
                datlen = rep[0].split(',')
                return True, int((datlen[2]))
        time.sleep(0.1)
        timeout -= 1
        if timeout <= 0:
            # print('[INFO]waitForResp Timeout')
            logging.error('waitForResp Timeout')
            return False, 0


# end of def waitForResp
def errcode_info(errcode):
    err_info = {61: '网络错误 net error',
                62: 'DNS 错误 DNS error',
                63: '连接错误 connect error',
                64: '超时 timeout65 服务器错误 server error',
                66: '操作禁止 operation not allowed',
                70: '应答错误 reply error',
                71: ' 用户错误 user error',
                72: ' 口令错误 password error',
                73: '类型错误 type error',
                74: '保持错误 rest error',
                75: '被动错误 passive error',
                76: '主动错误 active error',
                77: '操作错误 operate error',
                78: '上传错误 upload error',
                79: '下载错误 download error',
                }
    if errcode in err_info.keys():
        return err_info[errcode]
    else:
        return "未知，请报Bug"


def getPutLen():
    # 等待可以上传数据
    result = b''
    cmdTimeout = 10 * 5
    maxlen = 0
    while True:
        time.sleep(0.1)
        n = ser.inWaiting()
        if n:
            result += ser.read(n)
            recvInfo(result)
            i = result.find(b'\r\n+FTPPUT: 1,1,')
            if i == 0:
                i += len(b'\r\n+FTPPUT: 1,1,')
                e = result.find(b'\r\n', i)
                if e > i:
                    maxlen = int(result[i:e])
                    break
            i = result.find(b'\r\n+FTPPUT: 1,')
            if i == 0:
                i += len(b'\r\n+FTPPUT: 1,')
                e = result.find(b'\r\n', i)
                if e > i:
                    errcode = int(result[i:e])
                    # logging.debug(errcode_info(errcode))
                    logging.error('FTPPUT返回错误码：%d, 含义：%s' % (errcode, errcode_info(errcode)))
                    sys.exit(1)
        cmdTimeout -= 1
        if cmdTimeout < 0:
            # print('[INFO]等待"+FTPPUT:1,1,<maxlength>"上报超时')
            logging.error('等待"+FTPPUT:1,1,<maxlength>"上报超时')
            sys.exit()
    return maxlen


def ftpPutdat(putDat):
    # print('写FTP上传数据')

    putATcmd = b'AT+FTPPUT=2,' + str(len(putDat)).encode()

    putATcmdcrlf = putATcmd + b'\r\n'
    sendInfo(putATcmd)
    ser.write(putATcmdcrlf)

    # 允许上传数据长度
    cmdTimeout = 5 * 10
    cnlen = 0
    result = b''
    while True:
        n = ser.inWaiting()
        if n > 0:
            result += ser.read(n)
            recvInfo(result)
            i = result.find(b'\r\n+FTPPUT: 2,')
            if i > 0:
                i += len(b'\r\n+FTPPUT: 2,')
                e = result.find(b'\r\n', i)
                if e > i:
                    cnlen = int(result[i:e])
                    break
        time.sleep(0.1)
        cmdTimeout -= 1
        if cmdTimeout <= 0:
            # print('[INFO]FTPPUT timeout')
            logging.error('FTPPUT timeout')
            sys.exit()

    if cnlen < len(putDat):
        # print('[INFO]发送数据长度大于能够上传长度')
        logging.error('发送数据长度大于能够上传长度')
        sys.exit()

    sendInfo(putDat)
    if isinstance(putDat, str):
        ser.write(putDat.encode('utf-8'))
    else:
        ser.write(putDat)

    # 等待OK
    cmdTimeout = 1 * 10
    result = b''
    while True:
        n = ser.inWaiting()
        if n > 0:
            result += ser.read(n)
            recvInfo(result)
            i = result.find(b'\r\nOK\r\n')
            if i >= 0:
                i += len(b'\r\nOK\r\n')
                i = result.find(b'+FTPPUT: 1,1,', i)
                if i > 0:
                    i += len(b'+FTPPUT: 1,1,')
                    e = result.find(b'\r\n', i)
                    if e > i:
                        datlen = int(result[i:e])
                        return datlen  # 下次能发送数据长度
        time.sleep(0.1)
        cmdTimeout -= 1
        if cmdTimeout <= 0:
            # print('[INFO]等待PUT数据OK超时')
            logging.error('等待FTPPUT数据OK超时')
            sys.exit()


def isGetDataRdy():
    cmdTimeout = 20 * 10
    ptn = b'+FTPGET: 1,'
    dat = 99
    while True:
        n = ser.inWaiting()
        if n:
            result = ser.read(n)
            i = result.find(ptn)
            if i > 0:
                i += len(ptn)
                e = result.find(b'\r\n', i)
                dat = int(result[i:e])
                return dat
        time.sleep(0.1)
        cmdTimeout -= 1
        if cmdTimeout <= 0:
            # print('下载数据超时')
            logging.error('下载数据超时')
            sys.exit(0)


def printUsage():
    ver_msg = '''*******************************************************
Air720 AT命令测试工具（FTP命令测试）%s 
    ''' % ver_test_ftp
    msg = '''
用法：
       test_ftp COMx putfile getfile Server Username pwd path apn [baudrate]
      
    示例：
      test_ftp COM4 sscom51.ini sscom51-1.ini ftp.lhb.ink test testok / CMNET 115200
     
    参数说明：
        COMx - 串口号
        putfile-需要ftp上传的本地文件名，上传到ftp的名字也相同
        getfile-将上传的putfile下载后保存到本地的文件名，不要和putfile同名
        Server-ftp的地址
        Username-ftp用户名
        pwd-ftp用户密码
        path-ftp上传文件路径
        apn-使用的手机卡APN名称，移动卡为CMNET，联通卡为3GWAP
        baudrate-通讯波特率，可省略该参数,默认115200
    
    输出信息说明：
        带收←◆和发→◇的输出信息为收发的AT命令和数据
        其他开始的为程序运行信息
        运行信息保存在test_ftp_info.log(全部信息)和test_ftp_err.log(仅包含错误信息)
    
    测试工具说明：
        由于FTP的命令具有高度相关性， 所以需要按照一定顺序发送。由于发送和接收数据较
        多时，手动发送命令较难实现，且不便检查，所以需要自动测试工具来协助完成。
        
        主要测试流程是将本地文件上传到ftp服务器，然后下载下来，存到本地另一个文件中，
        最后比较这两个文件是否一致。
        
        测试的AT相关命令包括
            AT
            AT*EXASSERT=1
            AT+FTPQUIT
            AT+SAPBR=0,1
            AT+SAPBR=3, 1, "CONTYPE","GPRS"
            AT+SAPBR=3, 1, "APN","CMNET"
            AT+SAPBR=1,1
            AT+SAPBR=2,1
            AT+FTPCID=1
            AT+FTPSERV=
            AT+FTPUN=
            AT+FTPPW=
            AT+FTPPUTNAME=
            AT+FTPPUTPATH=
            AT+FTPPUT=1
            AT+FTPPUT=2,0
            AT+FTPGETNAME=
            AT+FTPGETPATH=
            AT+FTPGET=1
            AT+FTPGET=2,1460
**************************************************    
    '''
    print(ver_msg)
    print(msg)



if __name__ == '__main__':
    ver_test_ftp="v1.2"
    setup_logging(default_path="test_ftp.yaml", default_level=logging.INFO)
    logging.info("*************  Test FTP (%s) Start  ***********************" % ver_test_ftp)
    i = len(sys.argv)

    if i < 9:
        logging.info("Print usage...")
        printUsage()
        logging.info('*************   Test FTP (%s) End  ***********************'  % ver_test_ftp)
        sys.exit()
    comport = sys.argv[1]
    putfn = sys.argv[2]
    getfn = sys.argv[3]
    FTPSERV = sys.argv[4]
    FTPUN = sys.argv[5]
    FTPPW = sys.argv[6]
    ftppath = sys.argv[7]
    apn = sys.argv[8]
    usr_baudrate=115200
    if i==10:
        print(type(sys.argv[9]))
        usr_baudrate=int(sys.argv[9])
    logging.info("Use Comport:%s ,Buadrate=%s" % (comport,usr_baudrate))
    
    try:
        ser = serial.Serial(port=comport, baudrate=usr_baudrate, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=0)
        if ser.isOpen():
            # print("[INFO]%s open success" % comport)
            logging.info("%s open success" % comport)
        else:
            # print("[INFO]%s open failed" % comport)
            logging.error("%s open failed" % comport)
    except:
        # print ("[INFO]请查看是否成功打开设备端口："+comport)
        logging.error("请查看是否成功打开设备端口：" + comport)
        sys.exit()

    sendAT_Cmd(ser, 'AT\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT*EXASSERT=1\r\n', 1, 5)

    sendAT_Cmd(ser, 'AT+FTPQUIT\r\n', 0, 5)
    sendAT_Cmd(ser, 'AT+SAPBR=0,1\r\n', 0, 5)

    sendAT_Cmd(ser, 'AT+SAPBR=3, 1, "CONTYPE","GPRS"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+SAPBR=3, 1, "APN","' + apn + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+SAPBR=1,1\r\n', 0, 5)
    sendAT_Cmd(ser, 'AT+SAPBR=2,1\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPCID=1\r\n', 0, 5)

    sendAT_Cmd(ser, 'AT+FTPSERV="' + FTPSERV + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPUN="' + FTPUN + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPPW="' + FTPPW + '"\r\n', 1, 5)

    sendAT_Cmd(ser, 'AT+FTPPUTNAME="' + putfn + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPPUTPATH="' + ftppath + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPPUT=1\r\n', 1, 10)

    putlen = getPutLen()

    putf = open(putfn, 'rb')
    fsize = putf.seek(0, 2)
    putf.seek(0, 0)
    putsize = 0

    # 可以上传cnlen

    while True:
        putDat = putf.read(putlen)
        if len(putDat):
            putsize += len(putDat)
            putlen = ftpPutdat(putDat)
            # print('[INFO]文件大小：' +str(fsize)+',已上传:'+ str(putsize)+',上传比例: '+str(int((putsize/fsize)*100))+'%')
            msg = ('文件大小：' + str(fsize) + ',已上传:' + str(putsize) + ',上传比例: ' + str(int((putsize / fsize) * 100)) + '%')
            logging.info(msg)
        else:
            break

    # 结束FTP上传会话
    # print('[INFO]结束FTP上传会话')
    logging.info('结束FTP上传会话')
    # print ('['+time.strftime("%Y-%m-%d %X")+']发→◇'+'AT+FTPPUT=2,0\r\n')
    sendInfo('AT+FTPPUT=2,0\r\n')
    ser.write('AT+FTPPUT=2,0\r\n'.encode('utf-8'))

    cmdTimeout = 1 * 10
    found = False
    while True:

        n = ser.inWaiting()
        if n > 0:
            result = ser.read(n).decode()
            recvInfo(result)

            rep = result.split('\r\n')
            if 'OK' in rep:
                found = True
                break

        if found:
            break
        time.sleep(0.1)
        cmdTimeout -= 1
        if cmdTimeout < 0:
            # print('[INFO]结束FTPPUT超时')
            logging.error('结束FTPPUT超时')
            break
    if found:
        pass

    # wait for +FTPPUT:1,0
    cmdTimeout = 10 * 10
    found = False
    while True:
        n = ser.inWaiting()
        if n > 0:
            result = ser.read(n).decode()
            recvInfo(result)

            rep = result.split('\r\n')

            if '+FTPPUT: 1,0' in rep:
                found = True
                break

        if found:
            break
        time.sleep(0.1)
        cmdTimeout -= 1
        if cmdTimeout < 0:
            # print('[INFO]FTPPUT:1,0超时')
            logging.error('FTPPUT:1,0超时')
            sys.exit()

    # print('[INFO]下载文件测试')
    logging.info('开始下载文件测试')

    datfname = getfn  # #接收数据文件名称

    # print('[INFO]保存文件名：'+datfname)
    logging.info('保存文件名：' + datfname)

    datfile = open(datfname, 'wb')

    sendAT_Cmd(ser, 'AT+FTPGETNAME="' + putfn + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPGETPATH="' + ftppath + '"\r\n', 1, 5)
    sendAT_Cmd(ser, 'AT+FTPGET=1\r\n', 1, 5)

    getsize = 0
    dat = isGetDataRdy()
    if dat == 1:
        # print('[INFO]数据准备好可以下载')
        logging.info('FTPGET数据准备好可以下载')
    if dat == 0:
        # print('[INFO]数据下载完成')
        logging.info('数据下载完成')
    if dat > 1:
        # print('[INFO]数据下载出错,请检查FTPGET参数设置')
        logging.error('数据下载出错,请检查FTPGET参数设置')
        sys.exit()

    intDatLen = 1
    # print('[INFO]开始下载数据')
    logging.info('开始下载数据')
    while True:

        ser.write(b'AT+FTPGET=2,1460\r\n')
        sendInfo(b'AT+FTPGET=2,1460\r\n')

        cmdTimeout = 10 * 10
        result = b''
        while True:

            time.sleep(0.1)
            n = ser.inWaiting()
            if n:
                result += ser.read(n)
                if result.find(b'\r\nOK\r\n') == -1:
                    continue
                recvInfo(result)
                #
                pattern = b'+FTPGET: 2,'
                i = result.find(pattern)
                i = i + len(pattern)
                e = result.find(b'\r\n', i)
                intDatLen = int(result[i:e])
                de = result.find(b'\r\nOK\r\n', e + 2)
                dat = result[e + 2:de]
                getsize += intDatLen
                datfile.write(dat)
                msg = ('已下载字节数:%d' % getsize)
                logging.info(msg)

                break
            cmdTimeout -= 1
            if cmdTimeout <= 0:
                # print('[INFO]AT+FTPGET=2,1460 Timeout')
                logging.error('AT+FTPGET=2,1460 Timeout')
                sys.exit(0)
        if intDatLen == 0:
            # print('[INFO]下载完成')
            logging.info('下载完成')
            break

    # sendAT_Cmd(ser,'AT+FTPQUIT\r\n',1, 5)
    # sendAT_Cmd(ser,'AT+SAPBR=0,1\r\n',1, 5)

    datfile.close()

    if filecmp.cmp(putfn, getfn):
        # print('恭喜！上传文件'+putfn+'和下载文件'+getfn+'一致')
        logging.info('恭喜！上传文件' + putfn + '和下载文件' + getfn + '一致')
    else:
        print('[INFO]糟糕！上传文件' + putfn + '和下载文件' + getfn + '不一致')
        logging.error('糟糕！上传文件' + putfn + '和下载文件' + getfn + '不一致')

    ser.close()
    logging.info('**************** Test Ftp (%s) End *************'  % ver_test_ftp)
