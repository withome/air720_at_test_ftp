# 合宙Air720 4G模块AT指令FTP上传，下载功能测试工具

#### 介绍
使用Python3编写的Air720模块采用AT指令进行ftp上传和下载功能测试的工具和源码，供参考

#### 软件架构
打开串口
上传文件
下载文件
比较是否一致


#### 使用说明

Air720 AT命令测试工具（FTP命令测试）V1.0

用法：
      `test_ftp COMx putfile getfile Server Username pwd path apn  [baudrate]` 
      
    示例：
      `test_ftp COM4 sscom51.ini sscom51-1.ini ftp.lhb.ink test testok / CMNET 460800`
     
      *******
      为方便测试减少命令行输入参数，随测试工具提供两个bat文件：
           do.bat  包括图片和文本文件2个测试示例

       可以设置测试次数的bat文件:
           don.bat   [次数] [bat文件名-可省略]

             用法示例：  don 3   --运行do.bat 3次
                  
         *******                  

      
    参数说明：
        COMx - 串口号
        putfile-需要ftp上传的本地文件名，上传到ftp的名字也相同
        getfile-将上传的putfile下载后保存到本地的文件名，不要和putfile同名
        Server-ftp的地址
        Username-ftp用户名
        pwd-ftp用户密码
        path-ftp上传文件路径
        apn-使用的手机卡APN名称，移动卡为CMNET，联通卡为3GWAP
        baudrate-通讯波特率，可省略该参数,默认115200。如果更改波特率，先用sscom发AT+IPR=460800，
                 改到要用的波特率，再在命令行参数最后加上460800，即可使用

    
    输出信息说明：
        带收←◆和发→◇的输出信息为收发的AT命令和数据
        其他开始的为程序运行信息
        运行信息保存在test_ftp_info.log(全部信息)和test_ftp_err.log(仅包含错误信息)
    
    测试工具说明：
        由于FTP的命令具有高度相关性， 所以需要按照一定顺序发送。由于发送和接收数据较
        多时，手动发送命令较难实现，且不便检查，所以需要自动测试工具来协助完成。
        
        主要测试流程是将本地文件上传到ftp服务器，然后下载下来，存到本地另一个文件中，
        最后比较这两个文件是否一致。
        
        测试的AT相关命令包括
            AT
            AT*EXASSERT=1
            AT+FTPQUIT
            AT+SAPBR=0,1
            AT+SAPBR=3, 1, "CONTYPE","GPRS"
            AT+SAPBR=3, 1, "APN","CMNET"
            AT+SAPBR=1,1
            AT+SAPBR=2,1
            AT+FTPCID=1
            AT+FTPSERV=
            AT+FTPUN=
            AT+FTPPW=
            AT+FTPPUTNAME=
            AT+FTPPUTPATH=
            AT+FTPPUT=1
            AT+FTPPUT=2,0
            AT+FTPGETNAME=
            AT+FTPGETPATH=
            AT+FTPGET=1
            AT+FTPGET=2,1460
**************************************************    